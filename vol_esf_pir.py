# 24 de abril de 2018
# versi�n 1.0
# Daniel Nu�ez Murillo
# Iv�n Sol�s �vila
# Entradas: radio
# Salidas: volumen de la esfera

from math import pi
def vol_esf(r):
    if isinstance(r, int) and r > 0:
        return (4/3)*pi*r**3
    else:
        return "Error"

def vol_pir(h, l):
    if isinstance(h, int) and isinstance(l, int) and h > 0 and l > 0:
        return (l*l*h)/3
    else:
        return "Error" 
# comentario
